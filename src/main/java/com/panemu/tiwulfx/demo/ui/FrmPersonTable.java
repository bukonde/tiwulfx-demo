/*
 * License BSD License
 * Copyright (C) 2013 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.demo.ui;

import com.panemu.tiwulfx.common.TableData;
import com.panemu.tiwulfx.common.TiwulFXUtil;
import com.panemu.tiwulfx.control.LookupFieldController;
import com.panemu.tiwulfx.demo.DaoBase;
import com.panemu.tiwulfx.demo.misc.DataGenerator;
import com.panemu.tiwulfx.demo.misc.EmailValidator;
import com.panemu.tiwulfx.demo.misc.ProgressBarColumn;
import com.panemu.tiwulfx.demo.pojo.Insurance;
import com.panemu.tiwulfx.demo.pojo.Insurance_;
import com.panemu.tiwulfx.demo.pojo.Person;
import com.panemu.tiwulfx.demo.pojo.Person_;
import com.panemu.tiwulfx.dialog.MessageDialogBuilder;
import com.panemu.tiwulfx.table.ButtonColumn;
import com.panemu.tiwulfx.table.ComboBoxColumn;
import com.panemu.tiwulfx.table.LookupColumn;
import com.panemu.tiwulfx.table.NumberColumn;
import com.panemu.tiwulfx.table.TableControl;
import com.panemu.tiwulfx.table.TableController;
import com.panemu.tiwulfx.table.TextColumn;
import com.panemu.tiwulfx.table.TickColumn;
import com.panemu.tiwulfx.table.TypeAheadColumn;
import java.io.IOException;
import java.util.List;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;

/**
 *
 * @author Amrullah <amrullah@panemu.com>
 */
public class FrmPersonTable extends VBox {

    @FXML
    protected TableControl<Person> tblPerson;
	@FXML
	private TypeAheadColumn<Person, String> clmBirthPlace;
	@FXML
	private ComboBoxColumn<Person, Character> clmGender;
	@FXML
	private LookupColumn<Person, Insurance> clmInsurance;
	@FXML
	private ButtonColumn<Person> clmButton;
	@FXML
	private TextColumn<Person> clmEmail;
	@FXML
	private NumberColumn<Person, Integer> clmVisit;
	@FXML
	private NumberColumn<Person, Integer> clmInsuranceId;
	@FXML
	private NumberColumn<Person, Integer> clmVersion;
    private DaoBase<Insurance> daoInsurance = new DaoBase<>(Insurance.class);

    public FrmPersonTable(TableController<Person> controller) {
        FXMLLoader fxmlLoader = new FXMLLoader(FrmPersonTable.class.getResource("FrmPersonTable.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
		fxmlLoader.setResources(TiwulFXUtil.getLiteralBundle());
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        this.controller = controller;
        init(true);
        tblPerson.reloadFirstPage();
    }

    protected void init(boolean showTickColumn) {
        tblPerson.setRecordClass(Person.class);
        tblPerson.setController(controller);
        tblPerson.setMaxRecord(50);
		
        for (String location : DataGenerator.birthPlaces) {
            clmBirthPlace.addItem(location, location);
        }

        clmGender.addItem("Male", 'm');
        clmGender.addItem("Female", 'f');

		clmInsuranceId.setNumberType(Integer.class);
        clmInsurance.setLookupController(insuranceLookupController);
		clmEmail.addValidator(new EmailValidator());
		clmVisit.setNumberType(Integer.class);
		clmVersion.setNumberType(Integer.class);
        /**
         * Custom column. Not included in TiwulFX library
         */
        ProgressBarColumn<Person, Integer> clmProgress = new ProgressBarColumn<>(Person_.visit.getName());
        clmProgress.setEditable(false);
        clmProgress.setMax(5000);
		final ButtonColumn.ButtonColumnHelper<Person> buttonColumnHelper = new ButtonColumn.ButtonColumnHelper<Person>() {
			
			@Override
			public String getLabel(Person record) {
				return String.valueOf(record.getId());
			}

			@Override
			public void handle(Person record) {
				MessageDialogBuilder.info().message(record.getName()).show(null);
			}
		};

		clmButton.setHelper(buttonColumnHelper);
        if (showTickColumn) {
            final TickColumn<Person> clmTick = new TickColumn<>();
            clmTick.setDefaultTicked(true);
            tblPerson.getColumns().add(0, clmTick);
            Button btnGetTicked = new Button("Get Ticked Records");
            btnGetTicked.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    MessageDialogBuilder.info().title("Ticked Records")
                            .message(clmTick.getTickedRecords().size() + " ticked\n" + clmTick.getUntickedRecords().size() + " unticked")
                            .show(getScene().getWindow());
                }
            });
            tblPerson.addButton(btnGetTicked);
        }
        tblPerson.getColumns().add(11,clmProgress);
		
		MenuItem ctxMenu = new MenuItem("New Item");
		ctxMenu.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent t) {
				MessageDialogBuilder.info().message("New Item is clicked").show(getScene().getWindow());
			}
		});
		tblPerson.addContextMenuItem(ctxMenu);
    }
    private TableController<Person> controller;
    private LookupFieldController<Insurance> insuranceLookupController = new LookupFieldController<Insurance>(Insurance.class) {
        @Override
        public String[] getColumns() {
            return new String[]{
                        Insurance_.id.getName(),
                        Insurance_.code.getName(),
                        Insurance_.name.getName()
                    };
        }

        @Override
        protected String getWindowTitle() {
            return "Find Insurance";
        }

        @Override
        protected void initCallback(VBox container, TableControl<Insurance> table) {
            container.setPrefWidth(500);
            table.getTableView().getColumns().get(2).setPrefWidth(300);
        }

        @Override
        protected TableData loadData(int startIndex, List filteredColumns, List sortedColumns, List sortingTypes, int maxResult) {
            return daoInsurance.fetch(startIndex, filteredColumns, sortedColumns, sortingTypes, maxResult);
        }
    };
}
