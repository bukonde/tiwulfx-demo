/*
 * License BSD License
 * Copyright (C) 2013 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.demo.ui;

import com.panemu.tiwulfx.common.TableCriteria;
import com.panemu.tiwulfx.common.TableData;
import com.panemu.tiwulfx.demo.DaoBase;
import com.panemu.tiwulfx.demo.pojo.Person;
import com.panemu.tiwulfx.demo.pojo.Person_;
import com.panemu.tiwulfx.table.TableControl;
import com.panemu.tiwulfx.table.TableController;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.TableColumn;

/**
 *
 * @author Amrullah <amrullah@panemu.com>
 */
public class InRowMaintenanceController extends TableController<Person> {

    private DaoBase<Person> daoPerson = new DaoBase<>(Person.class);

    @Override
    public TableData loadData(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<TableColumn.SortType> sortingOrders, int maxResult) {
        boolean join = true;
//        for (TableCriteria crit : filteredColumns) {
//            if (crit.getAttributeName().equals(Person_.insurance.getName()) && crit.getOperator() == Operator.is_null) {
//                join = false;
//                break;
//            }
//        }
        List<String> lstJoin = new ArrayList<>();
        if (join) {
            lstJoin.add(Person_.insurance.getName());
        }
        TableData<Person> result = daoPerson.fetch(startIndex, filteredColumns, sortedColumns, sortingOrders, maxResult, lstJoin);
        return result;
    }

    @Override
    public List<Person> insert(List<Person> newRecords) {
        return daoPerson.insert(newRecords);
    }

    @Override
    public List<Person> update(List<Person> records) {
        List<Person> result = daoPerson.update(records);
        result = daoPerson.initRelationship(records, Person_.insurance.getName());
        return result;
    }

    @Override
    public void delete(List<Person> records) {
        daoPerson.delete(records);
    }

	@Override
	public void exportToExcel(String title, int maxResult, TableControl<Person> tblView, List<TableCriteria> lstCriteria) {
		super.exportToExcel("Person Data", maxResult, tblView, lstCriteria);
	}
	
	
}
